var mongoose = require("mongoose");

var reqDocCosDistSchema = new mongoose.Schema({


    reqA   : {type: mongoose.Schema.ObjectId, ref: 'Requisition' },
    reqB   : {type: mongoose.Schema.ObjectId, ref: 'Requisition' },
    dist   : [Number]

});

mongoose.model('ReqDocumentCosDist', reqDocCosDistSchema);
