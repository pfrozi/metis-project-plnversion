import nltk
import pymongo
import string

from pymongo import MongoClient
from sklearn.feature_extraction.text import TfidfVectorizer

client = MongoClient('localhost', 27017)

#define o banco de dados que serah utilizado
db = client['metis-db']

#define iterador
reqs     = db.requisitions
iterator = reqs.find()

filtered = [[w["_id"],w["description"]] for w in iterator]

#extrair descricao
filtered_descr = [w[1] for w in filtered]

#remocao de stopwords??

#tfidf calc
vect      = TfidfVectorizer(min_df=1)
tfidf     = vect.fit_transform(filtered_descr)

normalize = (tfidf * tfidf.T).A

#Inserir as distancias em na colecao ReqDocumentDistCos
#docs = [[w["_id"],w["description"]] for w in iterator]

distCos=[]
for x in range(0,len(normalize)-1):
    for y in range(0,len(normalize)-1):
        if(x<>y):
            dists={"reqA" : filtered[x][0], "reqB": filtered[y][0], "dist": normalize[x][y]}
            distCos.append(dists) 


#Realiza a insercao dos campos na colecao de distancias entre dois documentos
ReqDocumentDistCos = db.reqdocumentcosdists
ReqDocumentDistCos.insert_many(distCos)
